# coding:utf-8
from flask import Flask, jsonify, make_response
import time
import psutil
from novaclient import client
import os_client_config
app = Flask(__name__)


@app.route('/')
def index():
    return "H"


@app.route('/cpu')
def cpu_use():
    cpu_use = psutil.cpu_percent()
    mem_use = psutil.virtual_memory().percent
    timestamp = int(time.time())
    response = make_response(jsonify({"cpu_use": cpu_use, "mem_use": mem_use, "timestamp": timestamp}))
    response.headers['Access-Control-Allow-Origin'] = '*'
    return response, 200


@app.route('/net')
def net_info():
    net_data = {}
    net = psutil.net_io_counters()
    recv = net.bytes_recv
    sent = net.bytes_sent
    net_data["recv"] = recv
    net_data["sent"] = sent
    net_data["timestamp"] = int(time.time())
    response = make_response(jsonify(net_data))
    response.headers['Access-Control-Allow-Origin'] = '*'
    return response, 200


@app.route('/mem')
def memory():
    mem_data = {}


if __name__ == "__main__":
    app.run(host='0.0.0.0', debug=True)
