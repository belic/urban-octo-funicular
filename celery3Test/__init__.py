# coding=utf-8
# from __future__ import absolute_import
from celery import Celery

BROKER_URL = 'redis://localhost:6379/0'

app = Celery('hello', broker='redis://localhost:6379/0', backend='redis://localhost:6379/0')
app.autodiscover_tasks(['celery3Test.tasks'])  # 或在上面中使用include=[]参数
app.conf.update(CELERY_ENABLE_UTC=False, CELERY_TIMEZONE='Asia/Shanghai')


@app.task
def hello():
    return 'hello world'


if __name__ == '__main__':
    app.start()

"""
# 启动
celery -A celery3Test worker -l info  --autoreload

subtest.delay(9)
xsum.apply_async([[3,9,23,2]], countdown=5)
xsum.apply_async(([1,2,3],), link=tasks.add.s(9))

res = tasks.xsum.apply_async(([1,2,3],), countdown=10)
# PENDING -> STARTED -> RETRY -> STARTED -> RETRY -> STARTED -> SUCCESS
res.state / res.status  # PENDING  STARTED  SUCCESS FAILED
res.id / res.task_id
res.ready()
res.get()  # get result
"""
