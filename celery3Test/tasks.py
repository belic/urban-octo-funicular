# coding=utf-8
# from __future__ import absolute_import
from __future__ import unicode_literals
import time
from celery import shared_task, subtask
from celery.result import allow_join_result

from . import app


@app.task(name='Add Calc木')
def add(x, y):
    return x + y


@app.task(name='MulCalc')
def mul(x, y):
    return x * y


@app.task(name='sum')
def xsum(numbers):
    return sum(numbers)


@app.task(name='subTest要')
def subtest(a):
    if isinstance(a, (int, float)):
        ad = subtask('Add Calc木').delay(a, 3)
        m = mul.delay(a, 8)
        with allow_join_result():
            return 'TestComplete', ad.get(), m.get()
    return "参数a必须是int or float类型"


"""
tasks.xsum.apply_async(([1,2,3],), link=tasks.add.s((9,3)))
from celery3Test import app
app.tasks
tasks.subtest.apply_async(9)
from celery3Test import tasks
tasks.xsum.delay([1,2,3,4,5], link=tasks.add.s(3))
tasks.xsum.apply_async([[3,9,23,2]], countdown=5)
tasks.xsum.apply_async([[3,9,23,2]], countdown=12)
tasks.xsum.apply_async(([1,2,3],), link=tasks.add.s(9))
tasks.subtest.delay(9)
"""
