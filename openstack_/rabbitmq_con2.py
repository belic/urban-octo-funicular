import kombu
def connect(params):
    connection = kombu.Connection(hostname=params['host'])
    exchange = kombu.entity.Exchange(name=params['exchange_name'],
                                     type=params['exchange_type'],
                                     durable=params['exchange_durable'],
                                     auto_delete=params['exchange_auto_delete'],
                                     internal=params['exchange_internal'])
    queue_list = []
    for queue in params['queues_params']:
        queue_list.append(kombu.messaging.Queue(name=queue['name'],
                                                exchange=exchange,
                                                routing_key=queue['routing_key'],
                                                channel=connection.channel(),
                                                durable=queue['durable'],
                                                auto_delete=queue['auto_delete']))
    consumer = kombu.messaging.Consumer(channel=connection.channel(),
                                        queues=queue_list,
                                        no_ack=True,
                                        callbacks=[self._process_message])
    consumer.consume()
    return connection