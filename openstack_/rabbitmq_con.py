import pika
import json

credentials = pika.PlainCredentials('guest', 'guest')
params = pika.ConnectionParameters(host='10.1.1.60',credentials=credentials)
connection = pika.BlockingConnection(params)
channel = connection.channel()

exchange_name = 'nova'
queue_name = channel.queue_declare(exclusive=True).method.queue
binding_key = 'notifications.info'

channel.exchange_declare(exchange=exchange_name, exchange_type='topic')
channel.queue_bind(exchange=exchange_name,
                   queue=queue_name,
                   routing_key=binding_key)

print ' [*] Waiting for logs. To exit press CTRL+C'


def callback(ch, method, properties, body):
    # print(body)
    b = json.loads(body)
    b = json.loads(b['oslo.message'])

    print b['event_type'], b['payload']['state'], b['payload'].get('old_state')
    # print b
    # print "========================================================="
    print "========================================================="
# for key,value in b.iteritems():
# print key,':',value

channel.basic_consume(callback, queue=queue_name, no_ack=True)
channel.start_consuming()


