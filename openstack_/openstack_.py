# -*- coding: utf-8 -*-
import os
import time
from datetime import datetime

from keystoneauth1.identity import v3
from keystoneauth1 import session
from keystoneclient.v3 import client as ksclient
from novaclient import client as nvclient
from cinderclient import client as cdclient
from glanceclient import client as glclient

os.environ["OS_USERNAME"] = 'admin'
os.environ["OS_PASSWORD"] = 'Centos123'
os.environ["OS_AUTH_URL"] = 'http://controller01:35357/v3'
os.environ["OS_PROJECT_NAME"] = 'admin'
os.environ["OS_IDENTITY_API_VERSION"] = '3'
os.environ["OS_PROJECT_DOMAIN_NAME"] = 'Default'
os.environ["OS_USER_DOMAIN_NAME"] = "Default"

auth = v3.Password(user_domain_name=os.environ["OS_USER_DOMAIN_NAME"],
                   username='admin',
                   password='Centos123',
                   project_domain_name=os.environ["OS_PROJECT_DOMAIN_NAME"],
                   project_name=os.environ["OS_PROJECT_NAME"],
                   auth_url=os.environ["OS_AUTH_URL"])
sess = session.Session(auth=auth)
# print sess
# keystone = ksclient.Client(session=sess)
# print keystone.auth_token
nova = nvclient.Client('2', session=sess)
cinder = cdclient.Client('2', session=sess)
glance = glclient.Client('2', session=sess)
# print nova.servers.list()
RES_CREATE_TIMEOUT = 300  # 资源创建超时时间s
AVAILABILITY_ZONE = None  # 'nova','vdin01', '3' 不设了, 经常换还不通知, 受不了



if __name__ == '__main__':
    import chardet
    name = '这是一个中文2'.decode('utf-8')
    # print chardet.detect(name)

    nova.servers.update('c2c1c89f-a5a4-4505-910a-c0658f9a13b6', name=name)
