import sys
import logging as log
from kombu import BrokerConnection
from kombu import Exchange
from kombu import Queue
from kombu.mixins import ConsumerMixin

EXCHANGE_NAME = "nova"
ROUTING_KEY = "notifications.info"
QUEUE_NAME = ""
BROKER_URI="amqp://guest:guest@10.1.1.60:5672//"
# QUEUE_NAME = "compute"
# ROUTING_KEY = 'compute.compute02'
# BROKER_URI = "amqp://guest:guest@10.99.100.21:5672//"

log.basicConfig(stream=sys.stdout, level=log.INFO)


class NotificationsDump(ConsumerMixin):

    def __init__(self, connection):
        self.connection = connection
        return

    def get_consumers(self, consumer, channel):
        exchange = Exchange('cinder', type="topic", durable=False)
        queue = Queue(exchange=exchange, routing_key=ROUTING_KEY, durable=False, auto_delete=True, no_ack=True)
        return [consumer(queue, callbacks=[self.on_message], accept=['json'])]

    def on_message(self, body, message):
        log.info('Body: %r' % body)
        log.info('---------------')


if __name__ == "__main__":
    log.info("Connecting to broker {}".format(BROKER_URI))
    with BrokerConnection(BROKER_URI) as connection:
        NotificationsDump(connection).run()
