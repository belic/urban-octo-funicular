import pika


class MessageBroker:

    def __init__(self):
        queue_name = 'notifications.info'
        parameters = pika.URLParameters('amqp://guest:guest@10.1.1.60:5672//')
        connection = pika.BlockingConnection(parameters)

        channel = connection.channel()
        channel.queue_declare(queue=queue_name)
        print('[*] Waiting for message. To exit press CTRL+C')
        channel.basic_consume(self.callback, queue=queue_name, no_ack=True)
        channel.start_consuming()

    def callback(self, ch, method, properties, body):
        print body


def receive_message(self):
    pass
