# coding: utf-8
import os
import time
from datetime import datetime

from keystoneauth1.identity import v3
from keystoneauth1 import session
from keystoneclient.v3 import client as ksclient
from novaclient import client as nvclient
from cinderclient import client as cdclient
from glanceclient import client as glclient
from neutronclient.v2_0 import client as ntclient

os.environ["OS_USERNAME"] = 'admin'
os.environ["OS_PASSWORD"] = 'vdin1234'
os.environ["OS_AUTH_URL"] = 'http://controller:35357/v3'
os.environ["OS_PROJECT_NAME"] = 'admin'
os.environ["OS_IDENTITY_API_VERSION"] = '3'
os.environ["OS_PROJECT_DOMAIN_NAME"] = 'Default'
os.environ["OS_USER_DOMAIN_NAME"] = "Default"

auth = v3.Password(user_domain_name=os.environ["OS_USER_DOMAIN_NAME"],
                   username='admin',
                   password='vdin1234',
                   project_domain_name=os.environ["OS_PROJECT_DOMAIN_NAME"],
                   project_name=os.environ["OS_PROJECT_NAME"],
                   auth_url=os.environ["OS_AUTH_URL"])
sess = session.Session(auth=auth)
# print sess
# keystone = ksclient.Client(session=sess)
# print keystone.auth_token
nova = nvclient.Client('2', session=sess)
cinder = cdclient.Client('2', session=sess)
glance = glclient.Client('2', session=sess)
neutron = ntclient.Client(session=sess)